#!/usr/bin/env bash

CONFIG_DIR="$HOME/.everon"

if [ ! -d "$CONFIG_DIR" ]; then
  mkdir "$CONFIG_DIR"
fi

BACKEND_CRED_PATH="${CONFIG_DIR}/backend_cred.json"
GOOGLE_CRED_PATH="${CONFIG_DIR}/google_cred.json"

echo "Fetching Backend Credentials"
gcloud secrets versions access "latest" --secret="everon-creds" > "$BACKEND_CRED_PATH"
echo "Setting GOOGLE_BACKEND_CREDENTIALS Variable"
export GOOGLE_BACKEND_CREDENTIALS="$BACKEND_CRED_PATH"

#echo "Fetching Google default Credentials"
#gcloud secrets versions access "latest" --secret="everon-creds" > "$GOOGLE_CRED_PATH"
#echo "Setting GOOGLE_BACKEND_CREDENTIALS Variable"
#export GOOGLE_CREDENTIALS="$GOOGLE_CRED_PATH"