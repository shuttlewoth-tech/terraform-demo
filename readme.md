# Nginx Terraform Demo

This project demonstrates an implementation of a Terraform-controlled GKE cluster.

## Technology Used
* GKE
* GC HTTPS Balancer
* GKE Managed Certificates
* GC Cloud DNS

## Limitations
* Due to kubernetes_manifest requiring API access during the Plan phase, apply requires a two-stage approach.
* Without a FrontEnd config, Google Load balancer doe not redirect or restrict HTTP access to the service.

## Learnings
* Combining cluster creation and service deployment isn't ideal, at least until it is possible to deploy Kubernetes manifests without needing API access in Plan generation.
* When using Google HTTPS load balancers, special attention is required to how the load balancer health checks communicate with service endpoints.
* While GKE AutoPilot is very tempting for small-scale dev projects, its insistence on managing and updating resources can create friction, especially when updating deployments.
* Google Managed Certificates Work like a Charm. For small-scale deployments, it makes sense to use them due to their integration with GKE.
* Google does not expose load balancer health checks failing in GKE.

## Improvements
* Migrate the Nginx-Demo Module to a Helm Chart to make the Terraform Apply a seamless operation.
* Add Front End config to redirect all HTTP access to HTTPS
* Setup Google Cloud Armour

## How to use
This project assumes you have a separate Gcloud account with a working Cloud DNS managed zone. The credentials for the account hosting the GKE cluster are stored in the same account hosting the Domain using Secret Manager.

### Requirements 

* Two Google Cloud accounts
* A cloud DNS Managed Zone
* Account Credentials stored in Secret Manager for the account that will host GKE.

### Steps
1. Update `terraform.backend` to use with a valid state store
2. Set 'local.cloud_dns_managed_zone` to the name of a valid hosted zone.
3. Set `cloud_dns_managed_zone_project` to the same project that contains `local.cloud_dns_managed_zone`
4. Run `source ./secret-init.sh` to provision Google Cloud credentials Terraform will use.
5. Comment out `module.nginx-demo` and `google_dns_record_set.sub_domain` (Done due to Kubernetes provider limitations see Improvements section)
6. Run `terraform apply` to create the GKLE cluster.
7. Un-comment  `module.nginx-demo` and `google_dns_record_set.sub_domain`
8. Run `terraform apply`

# Terraform Docs

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google.cloud_dns"></a> [google.cloud\_dns](#provider\_google.cloud\_dns) | 4.2.0 |
| <a name="provider_random"></a> [random](#provider\_random) | 3.1.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_cluster"></a> [cluster](#module\_cluster) | ./modules/gke_cluster | n/a |
| <a name="module_nginx_demo"></a> [nginx\_demo](#module\_nginx\_demo) | ./modules/nginx_demo | n/a |

## Resources

| Name | Type |
|------|------|
| [google_dns_record_set.sub_domain](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/dns_record_set) | resource |
| [random_pet.cluster_prefix](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/pet) | resource |
| [google_dns_managed_zone.domain](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/dns_managed_zone) | data source |
| [google_secret_manager_secret_version.evbox_creds](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/secret_manager_secret_version) | data source |