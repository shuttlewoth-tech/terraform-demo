data "google_dns_managed_zone" "domain" {
  provider = google.cloud_dns
  name = local.cloud_dns_managed_zone
}

resource "google_dns_record_set" "sub_domain_traefik" {
  provider = google.cloud_dns
  name = "traefik.${local.env}.${data.google_dns_managed_zone.domain.dns_name}"

  managed_zone = data.google_dns_managed_zone.domain.name

  type = "A"
  ttl = "5"

  rrdatas = [module.traefik.ingress_ip]
}

resource "google_dns_record_set" "sub_domain" {
  provider = google.cloud_dns
  name = "${local.env}.${data.google_dns_managed_zone.domain.dns_name}"

  managed_zone = data.google_dns_managed_zone.domain.name

  type = "A"
  ttl = "5"

  rrdatas = [module.websocket_demo.ingress_ip]
}