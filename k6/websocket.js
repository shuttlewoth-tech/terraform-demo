import { randomString, randomIntBetween } from 'https://jslib.k6.io/k6-utils/1.2.0/index.js';
import ws from 'k6/ws';
import { Trend } from 'k6/metrics';
import { check, sleep } from 'k6';

const sessionDuration = randomIntBetween(10000, 20000); // user session between 10s and 1m

export const options = {
    vus: 200,
    iterations: 200,
};

export default function () {
    const url = `wss://everon-default.shuttleworth.tech/`;
    const params = { tags: { my_tag: 'my ws session' } };

    const res = ws.connect(url, params, function (socket) {

        socket.on('message', function (message) {
            console.log(message)
        })

        socket.setTimeout(function () {
            // console.log(`Closing the socket forcefully after ${sessionDuration / 1000}s`);
            socket.close();
        }, sessionDuration);
    });

    check(res, { 'Connected successfully': (r) => r && r.status === 101 });
}
