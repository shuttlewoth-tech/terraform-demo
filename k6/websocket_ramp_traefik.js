import { randomString, randomIntBetween } from 'https://jslib.k6.io/k6-utils/1.2.0/index.js';
import ws from 'k6/ws';
import { Trend } from 'k6/metrics';
import { check, sleep } from 'k6';

const sessionDuration = 60000;

export const options = {
    stages: [
        { duration: '1s', target: 20 },
        { duration: '10m', target: 20 },
    ]
};

export default function () {
    const url = `wss://traefik.evbox-demo.shuttleworth.tech/socket`;
    const params = { tags: { my_tag: 'my ws session' } };
    const connDuration = randomIntBetween(1000, sessionDuration)
    // const connDuration = sessionDuration

    const res = ws.connect(url, params, function (socket) {
        socket.on('open', function open() {
            socket.setInterval(function timeout() {
                socket.ping()
            }, 100);
        });

        socket.on('message', function (message) {
            console.log(message)
        })

        socket.on('error', function (e) {
            if (e.error() !== 'websocket: close sent') {
                console.log('An unexpected error occured: ', e.error());
            }
        });

        socket.setTimeout(function () {
            console.log(`Closing the socket forcefully after ${connDuration / 1000}s`);
            socket.close();
        }, connDuration);
    });

    // console.log(res.status)
    check(res, { 'Connected successfully': (r) => r && r.status === 101 });
}
