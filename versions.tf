terraform {
  required_version = ">= 1"

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 4.2.1"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.7.0"
    }
  }
}