locals {
  gke_network = "${local.env}-gke-network"
}

resource "google_compute_network" "gke" {
  name                    = local.gke_network
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "gke" {
  name          = local.gke_network
  ip_cidr_range = "10.63.0.0/20"
  region        = local.gcp_region
  network       = google_compute_network.gke.id
  secondary_ip_range {
    range_name    = "services-range"
    ip_cidr_range = "10.60.0.0/15"
  }

  secondary_ip_range {
    range_name    = "pod-ranges"
    ip_cidr_range = "10.63.240.0/20"
  }
}