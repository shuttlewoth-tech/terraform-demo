terraform {
  backend "http" {
  }
}

locals {
  project = "evbox"
  env = "${local.project}-${terraform.workspace}"
  is_prod = terraform.workspace == "prod"
  gcp_region = "europe-west1"
  gcp_zone = "${local.gcp_region}-d"
  cloud_dns_managed_zone = "shuttleworth-tech"
  cloud_dns_managed_zone_project = "shuttleworth-core"
}

resource "random_pet" "cluster_prefix" {
}

module "cluster" {
  source = "./modules/gke_cluster"
  depends_on = [google_compute_subnetwork.gke]
  cluster_prefix = random_pet.cluster_prefix.id
  env = local.env
  gcp_location = local.gcp_region
  is_prod = local.is_prod
  node_locations = [local.gcp_zone]
  network = google_compute_network.gke.name
  subnetwork = google_compute_subnetwork.gke.name
  cluster_secondary_range_name = google_compute_subnetwork.gke.secondary_ip_range.0.range_name
  services_secondary_range_name = google_compute_subnetwork.gke.secondary_ip_range.1.range_name
}

module "cluster_perms" {
  source = "./modules/cluster_perms"
  env = local.env
  is_prod = false
}

module "traefik" {
  source = "./modules/traefik"
  namespace = "traefik"
  is_prod = local.is_prod
  domain = trimsuffix(google_dns_record_set.sub_domain_traefik.name, ".")
  env = local.env
}

module "websocket_demo" {
  source = "./modules/websocket_demo_kong"
  namespace = "default"
  is_prod = local.is_prod
  domain = trimsuffix(google_dns_record_set.sub_domain.name, ".")
  env = local.env
}

module "prometheus" {
  depends_on = [module.cluster_perms]
  source = "./modules/prometheus"
  is_prod = local.is_prod
  domain = trimsuffix(google_dns_record_set.sub_domain_traefik.name, ".")
}
