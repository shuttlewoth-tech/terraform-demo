variable "namespace" {
  type = string
  description = "Kubernetes namespace to deploy"
}

variable "is_prod" {
  type = bool
}

variable "env" {
  type = string
}

variable "domain" {
  type = string
  description = "Domain name that the demo will be accessed from. EG bar.foo.com"
}