locals {
  grafana_values = {
    "grafana.ini" = {
      server = {
        domain = var.domain
        root_url = "%(protocol)s://%(domain)s/grafana"
        serve_from_sub_path = true
      }
    }
    sidecar = {
      dashboards = {
        enabled = true
      }
      datasources = {
        enabled = true
      }
    }
    admin = {
      existingSecret = kubernetes_secret.grafana_admin.metadata[0].name
    }
    autoscaling = {
      enabled = true
      minReplicas = 1
      maxReplicas = 10
    }
    service = {
      enabled = true
      type = "NodePort"
    }
    ingress = {
      enabled = true
      path = "/grafana"
      hosts = [var.domain]
      annotations = {
        "kubernetes.io/ingress.class" = "traefik"
      }
    }
  }
  prometheus_values = {
    grafana = {
      "grafana.ini" = {
        server = {
          domain = var.domain
          root_url = "%(protocol)s://%(domain)s/grafana"
          serve_from_sub_path = true
        }
      }
      admin = {
        existingSecret = kubernetes_secret.grafana_admin.metadata[0].name
      }
      autoscaling = {
        enabled = true
        minReplicas = 1
        maxReplicas = 10
      }
      service = {
        enabled = true
        type = "NodePort"
      }
      ingress = {
        enabled = true
        path = "/grafana"
        hosts = [var.domain]
        annotations = {
          "kubernetes.io/ingress.class" = "traefik"
        }
      }
      enabled = true
#      forceDeployDashboards = true
#      forceDeployDatasources = true
      defaultDashboardsEnabled = true
    }
    prometheus = {
      prometheusSpec = {
        serviceMonitorSelectorNilUsesHelmValues = false
        storageSpec = {
          volumeClaimTemplate = {
            spec = {
              storageClassName: "standard"
              accessModes = ["ReadWriteOnce"]
              resources = {
                requests = {
                  storage= "50Gi"
                }
              }
            }
          }
        }
      }
    }
  }
}

resource "random_password" "grafana_admin" {
  length = 10
}

resource "kubernetes_namespace" "prometheus" {
  metadata {
    name = var.namespace
  }
}

resource "helm_release" "prometheus" {
  repository = "https://prometheus-community.github.io/helm-charts"
  chart = "kube-prometheus-stack"
  name  = "prometheus-operator"
  namespace = kubernetes_namespace.prometheus.metadata[0].name
  values = [yamlencode(local.prometheus_values)]
  version = "36"
}

resource "kubernetes_secret" "grafana_admin" {
  metadata {
    name = "grafana-admin"
    namespace = kubernetes_namespace.prometheus.metadata[0].name
  }

  data = {
    admin-user = "teslasolari"
    admin-password = random_password.grafana_admin.result
  }
}