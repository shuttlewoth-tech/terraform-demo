locals {
  release_name = "ingress"
  chart_name = "traefik"
  traefik_metric_exposed_port = 9100
  traefik_web_exposed_port = 80
  traefik_web_port = 8000
  fullname = "${local.release_name}-${local.chart_name}"
  traefik_values = {
    ingressClass = {
      enabled = true
    }
    fullnameOverride = local.fullname
    deployment = {
      kind = "DaemonSet"
      podLabels = {
        app = "traefik"
      }
      podAnnotations = {
        "prometheus.io/scrape" = "true"
        "prometheus.io/port" = local.traefik_metric_exposed_port
      }
    }
    ports = {
      metrics = {
        expose = true
        exposedPort = local.traefik_metric_exposed_port
      }
      traefik = {
        expose = true
        exposedPort = 9000
        healthchecksPort = local.traefik_web_port
      }
      web = {
        expose = true
        exposedPort = local.traefik_web_exposed_port
      }
      websecure = {
        expose = false
        exposedPort = 433
      }
    }
    additionalArguments = [
      "--ping",
      "--ping.entrypoint=web"
    ]
    service = {
      type = "ClusterIP"
      annotations = {
        "cloud.google.com/neg" = "{\"ingress\": true}"
        "kubernetes.io/ingress.allow-http" = "false"
        "cloud.google.com/backend-config" = "{\"ports\": { \"${local.traefik_web_exposed_port}\":\"${kubernetes_manifest.backend_config.manifest.metadata.name}\"}}"
      }
    }
    autoscaling = {
      enabled = false
    }
  }
}

resource "kubernetes_namespace" "main" {
  metadata {
    name = var.namespace
  }
}

resource "google_compute_global_address" "ingress" {
  name = "kong-${var.env}"
}

resource "helm_release" "traefik" {
  repository = "https://helm.traefik.io/traefik"
  chart = "traefik"
  name  = "traefik"
  version = "10.21"
  values = [yamlencode(local.traefik_values)]
  namespace = kubernetes_namespace.main.metadata[0].name
}

resource "kubernetes_manifest" "ingress" {
  manifest = {
    apiVersion = "networking.k8s.io/v1"
    kind = "Ingress"
    metadata = {
      name = "${local.fullname}-ingress"
      namespace = kubernetes_namespace.main.metadata[0].name
      annotations = {
        "kubernetes.io/ingress.class": "gce"
        "kubernetes.io/ingress.global-static-ip-name" = google_compute_global_address.ingress.name
        "networking.gke.io/managed-certificates" = local.fullname
        "networking.gke.io/v1beta1.FrontendConfig" = local.fullname
      }
    }
    spec = {
      defaultBackend = {
        service = {
          name = local.fullname
          port = {
            number = local.traefik_values.ports.web.exposedPort
          }
        }
      }
    }
  }
}

resource "kubernetes_manifest" "managed_certificate" {
  manifest = {
    apiVersion = "networking.gke.io/v1"
    kind = "ManagedCertificate"
    metadata = {
      name = local.fullname
      namespace = kubernetes_namespace.main.metadata[0].name
    }
    spec = {
      domains = [var.domain]
    }
  }
}

resource "kubernetes_manifest" "frontend_config" {
  manifest = {
    apiVersion = "networking.gke.io/v1beta1"
    kind = "FrontendConfig"
    metadata = {
      name = local.fullname
      namespace = kubernetes_namespace.main.metadata[0].name
    }
    spec = {
      redirectToHttps = {
        enabled = true
        responseCodeName = "PERMANENT_REDIRECT"
      }
    }
  }
}

resource "kubernetes_manifest" "backend_config" {
  manifest = {
    apiVersion = "cloud.google.com/v1"
    kind = "BackendConfig"
    metadata = {
      name = local.fullname
      namespace = kubernetes_namespace.main.metadata[0].name
    }
    spec = {
      timeoutSec = 600
      logging = {
        enable = true
        sampleRate = 1
      }
      healthCheck = {
        port = local.traefik_web_port
        type = "HTTP"
        requestPath = "/ping"
      }
    }
  }
}

resource "kubernetes_manifest" "service_monitor" {
  manifest = {
    apiVersion = "monitoring.coreos.com/v1"
    kind = "ServiceMonitor"
    metadata = {
      name = local.fullname
      namespace = kubernetes_namespace.main.metadata[0].name
    }
    spec = {
      endpoints = [
        {
          scheme= "http"
          port = "metrics"
          path = "/metrics"
        }
      ]
      namespaceSelector = {
        matchNames = [ kubernetes_namespace.main.metadata[0].name ]
      }
      selector = {
        matchLabels = {
          "app.kubernetes.io/instance" = "traefik"
          "app.kubernetes.io/name" = "traefik"
        }
      }
    }
  }
}
