output "ingress_ip" {
  value = google_compute_global_address.ingress.address
}