
terraform {
  required_version = ">= 1"

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 4.2.1"
    }

    helm = {
      source = "hashicorp/helm"
      version = ">=2.5.1"
    }
  }
}