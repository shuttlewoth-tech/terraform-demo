locals {
  service_name = "websocket-demo"
  hellworld_values = {
    helloworld = {
      replicas = 2
      port: {
        container_port: 8080
      }
      staticIpName = google_compute_global_address.load_balancer.name
      domain = var.domain
    }
  }
}

resource "google_compute_global_address" "load_balancer" {
  name = "${local.service_name}-${var.env}-ingress"
}

resource "helm_release" "helloworld" {
  chart = "${path.module}/resources/chart"
  name  = "helloworld"
  values = [yamlencode(local.hellworld_values)]
  version = "0.1"
  force_update = true
}