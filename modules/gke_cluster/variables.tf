variable "env" {
  type = string
  description = "Variable used to differentiate the cluster purpose."
}

variable "is_prod" {
  type = bool
}

variable "cluster_prefix" {
  type = string
  description = "Name to append to cluster name."
}

variable "gcp_location" {
  type = string
  description = "The GCP Region the cluster will be created in."
}

variable "network" {
  type = string
  default = "default"
}

variable "subnetwork" {
  type = string
  default = "default"
}

variable "cluster_secondary_range_name" {
  type = string
}

variable "services_secondary_range_name" {
  type = string
}

variable "node_locations" {
  type = list(string)
  description = "List of GCP zones that nodes will be deployed in. Must be in the same region as gcp_location."
}