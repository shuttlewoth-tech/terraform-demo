locals {
  cluster_prefix = "${var.cluster_prefix}-${var.env}"
}


resource "google_container_cluster" "main" {
  name     = "${local.cluster_prefix}-cluster"
  location = var.gcp_location

  remove_default_node_pool = true
  initial_node_count       = 1

  network = data.google_compute_network.cluster.name
  subnetwork = data.google_compute_subnetwork.cluster.name

  release_channel {
    channel = var.is_prod ? "STABLE" : "REGULAR"
  }

  ip_allocation_policy {
    cluster_secondary_range_name  = "services-range"
    services_secondary_range_name = data.google_compute_subnetwork.cluster.secondary_ip_range.1.range_name
  }

  maintenance_policy {
    daily_maintenance_window {
      start_time = "02:00"
    }
  }
}

resource "google_container_node_pool" "main_nodes" {
  name     = "${local.cluster_prefix}-pool-main"
  location = var.gcp_location
  node_locations = var.node_locations
  cluster    = google_container_cluster.main.name
  initial_node_count = 1

  autoscaling {
    max_node_count = 20
    min_node_count = 1
  }

  node_config {
    preemptible  = true
    machine_type = "e2-standard-2"

    oauth_scopes    = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  }
}