data "google_compute_network" "cluster" {
  name = var.network
}

data "google_compute_subnetwork" "cluster" {
  name = var.subnetwork
}