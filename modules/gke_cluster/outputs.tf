data "google_container_cluster" "main" {
  name = google_container_node_pool.main_nodes.cluster
  project = google_container_node_pool.main_nodes.project
  location = google_container_node_pool.main_nodes.location
}

output "endpoint" {
  value = data.google_container_cluster.main.endpoint
}

output "cluster_ca_certificate" {
  value = data.google_container_cluster.main.master_auth[0].cluster_ca_certificate
}
