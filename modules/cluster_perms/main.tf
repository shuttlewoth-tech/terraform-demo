data "google_client_openid_userinfo" "default" {}

resource "kubernetes_cluster_role_binding" "terraform_admin" {
  metadata {
    name = "terraform-admin"
  }

  subject {
    kind = "User"
    name = data.google_client_openid_userinfo.default.email
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "admin"
  }
}

resource "kubernetes_cluster_role_binding" "terraform_additional" {
  metadata {
    name = "terraform-additional"
  }

  subject {
    kind = "User"
    name = data.google_client_openid_userinfo.default.email
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role.terraform_additional.metadata[0].name
  }
}

resource "kubernetes_cluster_role" "terraform_additional" {
  metadata {
    name = "terraform-additional"
  }

  rule {
    api_groups = ["rbac.authorization.k8s.io"]
    resources  = ["clusterroles"]
    verbs      = ["delete", "create"]
  }

  rule {
    api_groups = ["rbac.authorization.k8s.io"]
    resources  = ["clusterrolebindings"]
    verbs      = ["delete", "create"]
  }

  rule {
    api_groups = ["rbac.authorization.k8s.io"]
    resources  = ["rolebindings"]
    verbs      = ["delete", "create"]
  }

  rule {
    api_groups = ["rbac.authorization.k8s.io"]
    resources  = ["roles"]
    verbs      = ["delete", "create"]
  }
}