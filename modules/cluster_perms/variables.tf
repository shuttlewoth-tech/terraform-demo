variable "env" {
  type = string
  description = "Variable used to differentiate the cluster purpose."
}

variable "is_prod" {
  type = bool
}