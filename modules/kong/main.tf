locals {
  release_name = "ingress"
  chart_name = "kong"
  fullname = "${local.release_name}-${local.chart_name}"
  kong_proxy_port = 8000
  status_port = 8100
  kong_values = {
    env = {
      database = "off"
    }
    deployment = {
      daemonset = true
    }
    ingressController = {
      installCRDs = false
    }
    serviceMonitor = {
      enabled = true
    }
    autoscaling = {
      enabled = true
    }
    status = {
      enaled = true
      http = {
        enabled = true
        containerPort  = local.status_port
      }
    }
    admin = {
      enabled = true
      tls = {
        parameters = []
      }
    }
    cluster = {
      enabled = true
    }
    proxy = {
      type = "ClusterIP"
      enabled = true
      annotations = {
      }
      http = {
        enabled = true
        containerPort = local.kong_proxy_port
      }
      tls = {
        enabled = false
      }
      ingress = {
        enabled = false
        annotations = {
#          "kubernetes.io/ingress.class": "gce"
#          "kubernetes.io/ingress.global-static-ip-name" = google_compute_global_address.ingress.name
#          "networking.gke.io/managed-certificates" = local.fullname
#          "networking.gke.io/v1beta1.FrontendConfig" = local.fullname
        }
      }
    }
  }
}

resource "google_compute_global_address" "ingress" {
  name = "kong-${var.env}"
}

resource "kubernetes_namespace" "kong" {
  metadata {
    name = var.namespace
  }
}

resource "kubernetes_manifest" "prometheus_plugin" {
  manifest = {
    apiVersion = "configuration.konghq.com/v1"
    kind = "KongClusterPlugin"
    metadata = {
      name = "prometheus-kong-plugin"
      annotations = {
        "kubernetes.io/ingress.class" = "kong"
      }
      labels = {
        global = "true"
      }
    }
    plugin = "prometheus"
  }
}

resource "kubernetes_manifest" "managed_certificate" {
  manifest = {
    apiVersion = "networking.gke.io/v1"
    kind = "ManagedCertificate"
    metadata = {
      name = local.fullname
      namespace = kubernetes_namespace.kong.metadata[0].name
    }
    spec = {
      domains = [var.domain]
    }
  }
}

resource "kubernetes_manifest" "frontend_config" {
  manifest = {
    apiVersion = "networking.gke.io/v1beta1"
    kind = "FrontendConfig"
    metadata = {
      name = local.fullname
      namespace = kubernetes_namespace.kong.metadata[0].name
    }
    spec = {
      redirectToHttps = {
        enabled = true
        responseCodeName = "PERMANENT_REDIRECT"
      }
    }
  }
}

resource "kubernetes_manifest" "backend_config" {
  manifest = {
    apiVersion = "cloud.google.com/v1"
    kind = "BackendConfig"
    metadata = {
      name = local.fullname
      namespace = kubernetes_namespace.kong.metadata[0].name
    }
    spec = {
      timeoutSec = 600
      logging = {
        enable = true
        sampleRate = 1
      }
      healthCheck = {
        port = local.kong_values.status.http.containerPort
        type = "HTTP"
        requestPath = "/status"
      }
    }
  }
}

resource "kubernetes_manifest" "kong_proxy_service" {
  manifest = {
    apiVersion = "v1"
    kind = "Service"
    metadata = {
      name = "${local.fullname}-service"
      namespace = kubernetes_namespace.kong.metadata[0].name
      annotations = {
        "cloud.google.com/neg" = "{\"ingress\": true}"
#        "controller.autoneg.dev/neg" =  "{\"backend_services\":{\"80\":[{\"name\":\"${local.fullname}\"}]}}"
        "kubernetes.io/ingress.allow-http" = "false"
        "cloud.google.com/backend-config" = "{\"ports\": { \"${local.kong_proxy_port}\":\"${kubernetes_manifest.backend_config.manifest.metadata.name}\", \"${local.status_port}\":\"${kubernetes_manifest.backend_config.manifest.metadata.name}\"}}"
      }
    }
    spec = {
      ports = [
        {
          name = "kong-proxy"
          port = 8000
          protocol = "TCP"
          targetPort = "proxy"
        },
        {
          name = "kong-proxy-status"
          port = 8100
          protocol = "TCP"
          targetPort = "status"
        }
      ]
      selector = {
        "app.kubernetes.io/component" = "app"
        "app.kubernetes.io/instance" = local.release_name
        "app.kubernetes.io/name" = local.chart_name
      }
      type = "ClusterIP"
    }
  }
}

resource "kubernetes_manifest" "kong_proxy_ingress" {
  manifest = {
    apiVersion = "networking.k8s.io/v1"
    kind = "Ingress"
    metadata = {
      name = "${local.fullname}-ingress"
      namespace = kubernetes_namespace.kong.metadata[0].name
      annotations = {
        "kubernetes.io/ingress.class": "gce"
        "kubernetes.io/ingress.global-static-ip-name" = google_compute_global_address.ingress.name
        "networking.gke.io/managed-certificates" = local.fullname
        "networking.gke.io/v1beta1.FrontendConfig" = local.fullname
      }
    }
    spec = {
      defaultBackend = {
        service = {
          name = kubernetes_manifest.kong_proxy_service.manifest.metadata.name
          port = {
            number = local.kong_proxy_port
          }
        }
      }
      rules = [{
        http = {
          paths = [
            {
              backend = {
                service = {
                  name = kubernetes_manifest.kong_proxy_service.manifest.metadata.name
                  port = {
                    number = local.status_port
                  }
                }
              }
              pathType = "Prefix"
              path = "/status"
            },
          ]
        }
      }]
    }
  }
}

resource "helm_release" "kong" {
  repository = "https://charts.konghq.com"
  chart = local.chart_name
  name  = local.release_name
  namespace = kubernetes_namespace.kong.metadata[0].name
  values = [yamlencode(local.kong_values)]
  version = "2.9"
}