variable "namespace" {
  type = string
  default = "prometheus"
  description = "Kubernetes namespace to deploy"
}

variable "is_prod" {
  type = bool
}

variable "domain" {
  type = string
  description = "Domain name that the demo will be accessed from. EG bar.foo.com"
}

variable "env" {
  type = string
}