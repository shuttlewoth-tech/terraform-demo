provider "google" {
  alias = "cloud_dns"
  project = local.cloud_dns_managed_zone_project
}

provider "google" {
  credentials = data.google_secret_manager_secret_version.evbox_creds.secret_data
  project = "everon-test15"
  region = local.gcp_region
  zone = local.gcp_zone
}

data "google_client_config" "default" {}

provider "helm" {
  kubernetes {
    host  = "https://${module.cluster.endpoint}"
    token = data.google_client_config.default.access_token
    cluster_ca_certificate = base64decode(module.cluster.cluster_ca_certificate)
  }
}

provider "kubernetes" {
  host  = "https://${module.cluster.endpoint}"
  token = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.cluster.cluster_ca_certificate)
}