data "google_secret_manager_secret_version" "evbox_creds" {
  provider = google.cloud_dns
  project = local.cloud_dns_managed_zone_project
  secret = "everon-creds"
}